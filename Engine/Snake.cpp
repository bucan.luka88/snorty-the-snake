#include "Snake.h"

Snake::Snake(const Location& in_loc)
	:
	loc(in_loc)
{
	segments[0] = Segments(loc, headC);
}

void Snake::draw(const Board& brd) const
{
	for (int i = 1; i < nSize; i++) segments[i].draw(brd);
	brd.drawCell(loc, headC);
}

void Snake::input(const Keyboard& kbd, float dt)
{
	if (kbd.KeyIsPressed(VK_UP) && dir.y == 0 && hasMoved)
	{
		dir = { 0, -1 };
		hasMoved = false;
	}
	if (kbd.KeyIsPressed(VK_DOWN) && dir.y == 0 && hasMoved)
	{
		dir = { 0, 1 };
		hasMoved = false;
	}
	if (kbd.KeyIsPressed(VK_LEFT) && dir.x == 0 && hasMoved)
	{
		dir = { -1, 0 };
		hasMoved = false;
	}
	if (kbd.KeyIsPressed(VK_RIGHT) && dir.x == 0 && hasMoved)
	{
		dir = { 1, 0 };
		hasMoved = false;
	}
	if (kbd.KeyIsPressed(VK_CONTROL)) moveTimer += 2.0f * dt;
}

void Snake::update(float dt, Board& brd)
{
	moveTimer += dt;
	if (moveTimer >= movePeriod)
	{
		moveTimer -= movePeriod;
		//Check if Snecko died
		if (!brd.isInsideBoard(getNextLoc())) isDead = true;
		else if (brd.checkObstacle(getNextLoc())) isDead = true;
		for (int i = 1; i < nSize - 1; i++)
		{
			if (getNextLoc() == segments[i].getLoc())
			{
				isDead = true;
				break;
			}
		}
		//Move Snecko if not dead
		if (!isDead)
		{


			for (int i = nSize; i > 0; i--)
			{
				segments[0].follow(loc);
				segments[i].follow(segments[i - 1].getLoc());
			}
			loc += dir;
			hasMoved = true;
			if (brd.checkGoal(loc))
			{
				brd.eatGoal(loc);
				grow();
				score++;
			}
			else if (brd.checkCoca(loc))
			{
				brd.eatCoca(loc);
				speedUp();
			}
		}
	}
	highscore = std::max(score, highscore);
}

const Location& Snake::getLoc() const
{
	return loc;
}

const Location Snake::getNextLoc() const
{
	return loc + dir;
}

const bool Snake::bDead() const
{
	return isDead;
}

void Snake::speedUp()
{
	movePeriod *= speedUpMultiplier;
}

int Snake::getScore() const
{
	return score;
}

int Snake::getHScore() const
{
	return highscore;
}

void Snake::grow()
{
	Color tailC[4] = { {0, 100, 0}, {0, 130, 0}, {0, 160, 0}, {0, 130, 0} };
	segments[nSize] = Segments(segments[nSize - 1].getLoc(), tailC[(nSize - 1)%4]);
	nSize++;
}

void Snake::restart(const Board& brd)
{
	nSize = 1;
	dir = { 0, 0 };
	loc = { brd.getWidth() / 2, brd.getHeight() / 2 };
	movePeriod = 0.4f;
	isDead = false;
	hasMoved = false;
	score = 0;
}

Snake::Segments::Segments(const Location& in_loc, const Color& in_c)
	:
	loc(in_loc),
	c(in_c)
{
}

void Snake::Segments::draw(const Board& brd) const
{
	brd.drawCell(loc, c);
}

void Snake::Segments::follow(const Location& next_loc)
{
	loc = next_loc;
}

const Location& Snake::Segments::getLoc() const
{
	return loc;
}

