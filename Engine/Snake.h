#pragma once
#include "Board.h"
#include "Keyboard.h"
class Snake
{
public:
	Snake(const Location& in_loc);
	void draw(const Board& brd) const;
	void input(const Keyboard& kbd, float dt);
	void update(float dt, Board& brd);
	const Location& getLoc() const;
	const Location getNextLoc() const;
	const bool bDead() const;
	void speedUp();
	int getScore() const;
	int getHScore() const;
	void grow();
	void restart(const Board& brd);

private:
	class Segments
	{
	public:
		Segments() = default;
		Segments(const Location& in_loc, const Color& in_c);
		void draw(const Board& brd) const;
		void follow(const Location& next_loc);
		const Location& getLoc() const;
	private:
		Location loc;
		Color c;
	};

private:
	Location loc;
	Location dir = { 0, 0 };
	static constexpr Color headC = Colors::Yellow;
	float movePeriod = 0.4f;
	float moveTimer = 0.0f;
	static constexpr float speedUpMultiplier = 0.975f; //2.5% speedup
	bool isDead = false;
	bool hasMoved = false;
	int score = 0;
	int highscore = 0;

	//For Segments
	static constexpr int nSizeMax = 100;
	int nSize = 1;
	Segments segments[nSizeMax];
};

