/****************************************************************************************** 
 *	Chili DirectX Framework Version 16.07.20											  *	
 *	Game.cpp																			  *
 *	Copyright 2016 PlanetChili.net <http://www.planetchili.net>							  *
 *																						  *
 *	This file is part of The Chili DirectX Framework.									  *
 *																						  *
 *	The Chili DirectX Framework is free software: you can redistribute it and/or modify	  *
 *	it under the terms of the GNU General Public License as published by				  *
 *	the Free Software Foundation, either version 3 of the License, or					  *
 *	(at your option) any later version.													  *
 *																						  *
 *	The Chili DirectX Framework is distributed in the hope that it will be useful,		  *
 *	but WITHOUT ANY WARRANTY; without even the implied warranty of						  *
 *	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the						  *
 *	GNU General Public License for more details.										  *
 *																						  *
 *	You should have received a copy of the GNU General Public License					  *
 *	along with The Chili DirectX Framework.  If not, see <http://www.gnu.org/licenses/>.  *
 ******************************************************************************************/
#include "MainWindow.h"
#include "Game.h"

Game::Game( MainWindow& wnd )
	:
	wnd( wnd ),
	gfx( wnd ),
    brd( gfx),
    snake( {brd.getWidth()/2, brd.getHeight()/2})
{
}

void Game::Go()
{
	gfx.BeginFrame();	
	UpdateModel();
	ComposeFrame();
	gfx.EndFrame();
}

void Game::UpdateModel()
{
    float dt = ft.Mark();
    bGameRunning = !snake.bDead();
    if (bGameRunning)
    {
        snake.input(wnd.kbd, dt);
        snake.update(dt, brd);

        brd.spawnObstacle(dt, snake);
    }
    else if (wnd.kbd.KeyIsPressed(VK_ESCAPE)) restart();
}

void Game::restart()
{
    brd.restart();
    snake.restart(brd);
}

void Game::ComposeFrame()
{
    brd.draw();
    snake.draw(brd);

    img::score(50, 10, gfx);
    score.Draw(165, 10, snake.getScore(), Colors::White, gfx);
    img::hscore(525, 10, gfx);
    hscore.Draw(720, 10, snake.getHScore(), Colors::White, gfx);
}
