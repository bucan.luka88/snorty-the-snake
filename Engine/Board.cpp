#include "Board.h"
#include "Snake.h"

Board::Board(Graphics& gfx)
	:
	gfx(gfx)
{
	int goal_i;
	int coca_i;
	int center_i = (height / 2) * width + (width / 2);
	//Spawn Goals
	while (nGoals < nGoalsMax)
	{
		goal_i = rng::rdm_int(0, nCells);
		if (!hasGoal[goal_i] && goal_i != center_i)
		{
			hasGoal[goal_i] = true;
			nGoals++;
		}
	}
	//Spawn Coca {brd.getWidth()/2, brd.getHeight()/2}
	while (nCoca < nCocaMax)
	{
		coca_i = rng::rdm_int(0, nCells);
		if (!hasCoca[coca_i] && !hasGoal[coca_i] && coca_i != center_i)
		{
			hasCoca[coca_i] = true;
			nCoca++;
		}
	}
}

void Board::drawCell(const Location& loc, Color c) const
{
	gfx.DrawRectDim({ offset.x + loc.x * celldim + 1, offset.y + loc.y * celldim + 1 }, celldim - 2, celldim - 2, c);
}

void Board::draw()
{
	gfx.DrawRectEmpty(offset.x - borderThickness, offset.y - borderThickness, 
		width * celldim + borderThickness * 2, height * celldim + borderThickness * 2, borderThickness, borderC);
	for (int y = 0; y < height; y++)
	{
		for (int x = 0; x < width; x++)
		{
			if (hasCoca[y * width + x]) drawCell({ x, y }, cocaC);
			if (hasObstacle[y * width + x]) drawCell({ x, y }, borderC);
			if (hasGoal[y * width + x]) drawCell({ x, y }, goalC);
			
		}
	}
}

int Board::getWidth() const
{
	return width;
}

int Board::getHeight() const
{
	return height;
}

bool Board::isInsideBoard(const Location& loc) const
{
	return loc.x >= 0 && loc.x < width && loc.y >=0 && loc.y < height;
}

void Board::spawnObstacle(float dt, const Snake& snake)
{
	obstTimer += dt;
	int i = rng::rdm_int(0, nCells);
	int snakeH_i = snake.getLoc().y * width + snake.getLoc().x;
	int snakeHnext_i = snake.getNextLoc().y * width + snake.getNextLoc().x;
	if (obstTimer >= obstPeriod)
	{
		obstTimer -= obstPeriod;
		while (hasObstacle[i] || hasCoca[i] || hasGoal[i] || i == snakeH_i || i == snakeHnext_i) i = rng::rdm_int(0, nCells);
		hasObstacle[i] = true;
	}
}

void Board::eatGoal(const Location& loc)
{
	hasGoal[loc.y * width + loc.x] = false;
	int i = rng::rdm_int(0, nCells);
	while (hasObstacle[i] || hasCoca[i] || hasGoal[i]) i = rng::rdm_int(0, nCells);
	hasGoal[i] = true;
}

void Board::eatCoca(const Location& loc)
{
	hasCoca[loc.y * width + loc.x] = false;
}

bool Board::checkObstacle(const Location& loc) const
{
	return hasObstacle[loc.y * width + loc.x];
}

bool Board::checkGoal(const Location& loc) const
{
	return hasGoal[loc.y * width + loc.x];
}

bool Board::checkCoca(const Location& loc) const
{
	return hasCoca[loc.y * width + loc.x];
}

void Board::restart()
{
	for(int i = 0; i < nCells; i++)
	{ 
		hasObstacle[i] = { false };
		hasGoal[i] = { false };
		hasCoca[i] = { false };
	}

	obstTimer = 0.0f;
	nGoals = 0;
	nCoca = 0;

	int goal_i;
	int coca_i;
	int center_i = (height / 2) * width + (width / 2);
	//Spawn Goals
	while (nGoals < nGoalsMax)
	{
		goal_i = rng::rdm_int(0, nCells);
		if (!hasGoal[goal_i] && goal_i != center_i)
		{
			hasGoal[goal_i] = true;
			nGoals++;
		}
	}
	//Spawn Coca {brd.getWidth()/2, brd.getHeight()/2}
	while (nCoca < nCocaMax)
	{
		coca_i = rng::rdm_int(0, nCells);
		if (!hasCoca[coca_i] && !hasGoal[coca_i] && coca_i != center_i)
		{
			hasCoca[coca_i] = true;
			nCoca++;
		}
	}
}
