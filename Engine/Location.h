#pragma once

struct Location
{
	bool operator== (const Location& rhs) const
	{
		return x == rhs.x && y == rhs.y;
	}
	Location operator+ (const Location& rhs) const
	{
		return { x + rhs.x, y + rhs.y };
	}
	Location& operator+= (const Location& rhs)
	{
		return *this = *this + rhs;
	}
	int x, y;
};
