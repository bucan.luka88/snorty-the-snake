#pragma once
#include "Graphics.h"
#include "Location.h"
#include "rng.h"
class Board
{
public:
	Board(Graphics& gfx);
	void drawCell(const Location& loc, Color c) const;
	void draw();
	int getWidth() const;
	int getHeight() const;
	bool isInsideBoard(const Location& loc) const;
	void spawnObstacle(float dt, const class Snake& snake);
	void eatGoal(const Location& loc);
	void eatCoca(const Location& loc);
	bool checkObstacle(const Location& loc) const;
	bool checkGoal(const Location& loc) const;
	bool checkCoca(const Location& loc) const;
	void restart();

private:
	static constexpr int width = 35;
	static constexpr int height = 25;
	static constexpr int celldim = 20;
	static constexpr Location offset = { 50, 70 };
	static constexpr int borderThickness = 5;
	static constexpr Color borderC = Colors::Gray;
	static constexpr Color goalC = Colors::Red;
	static constexpr Color cocaC = Colors::MakeRGB(100, 40, 140);
	static constexpr int nCells = width * height;
	bool hasObstacle[nCells] = { false };
	bool hasGoal[nCells] = { false };
	bool hasCoca[nCells] = { false };
	static constexpr float obstPeriod = 2.5f;
	float obstTimer = 0.0f;
	static constexpr int nGoalsMax = 12;
	static constexpr int nCocaMax = (int)((float)(width * height) / 2.5f);
	int nGoals = 0;
	int nCoca = 0;
	Graphics& gfx;
};

